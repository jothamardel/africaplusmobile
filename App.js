import React from "react";
import { StatusBar as ExpoStatusBar } from "expo-status-bar";
import { ThemeProvider } from "styled-components/native";
import { theme } from "./src/infrastructure/theme";
import {
  useFonts as useOswald,
  Oswald_400Regular,
} from "@expo-google-fonts/oswald";
import { useFonts as useLato, Lato_400Regular } from "@expo-google-fonts/lato";

// import { RestaurantContextProvider } from "./src/services/restaurants/restaurants.context";
// import { LocationContextProvider } from "./src/services/location/location.context";
// import { Navigation } from "./src/infrastructure/navigation";
import { LoginScreen } from "./src/features/login/screen/login.screen";

export default function App(props) {
  const [OswaldLoaded] = useOswald({
    Oswald_400Regular,
  });
  const [LatoLoaded] = useLato({
    Lato_400Regular,
  });

  if (!OswaldLoaded || !LatoLoaded) {
    return null;
  }

  return (
    <>
      <ThemeProvider theme={theme}>
        <LoginScreen />
      </ThemeProvider>
      <ExpoStatusBar style="auto" />
    </>
  );
}
