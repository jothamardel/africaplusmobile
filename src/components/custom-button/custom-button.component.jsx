import React from "react";
import { Pressable, TouchableOpacity } from "react-native";
import styled from "styled-components";

const ButtonContainer = styled(TouchableOpacity)`
  border: 1px solid ${(props) => props.theme.colors.text.primary};
  padding: ${(props) => props.theme.space[3]};
  border-radius: ${(props) => props.theme.space[4]};
`;
const ButtonTitle = styled.Text`
  color: ${(props) => props.theme.colors.text.primary};
  text-align: center;
  font-weight: ${(props) => props.theme.fontWeights.medium};
`;

export const CustomButtom = ({ children, ...otherProps }) => {
  return (
    <ButtonContainer {...otherProps}>
      <ButtonTitle>{children}</ButtonTitle>
    </ButtonContainer>
  );
};
