import React from "react";
import { TextInput } from "react-native-paper";
import styled, { useTheme } from "styled-components";

const Input = styled(TextInput)`
  background: ${(props) => props.theme.colors.bg.primary};
  margin-bottom: ${(props) => props.theme.space[3]};
`;

const Title = styled.Text`
  color: ${(props) => props.theme.colors.text.primary};
`;

export const CustomInput = ({
  icon = "",
  iconColor = "",
  placeholder = "",
  title = "",
  ...otherProps
}) => {
  const { colors } = useTheme();

  return (
    <>
      <Input
        label={<Title>{title}</Title>}
        placeholder={placeholder}
        underlineColor={colors.brand.secondary}
        activeUnderlineColor={colors.brand.secondary}
        right={icon && <TextInput.Icon icon={icon} color={iconColor} />}
        {...otherProps}
      />
    </>
  );
};
