import React from "react";
import styled from "styled-components";
import { CustomButtom } from "../../../components/custom-button/custom-button.component";
import { CustomInput } from "../../../components/custom-input/custom-input.component";
import { Spacer } from "../../../components/spacer/spacer.component";
import { SafeArea } from "../../../components/utility/safe-area.component";

const LoginContainer = styled.View`
  display: flex;
  justify-content: center;
`;

const Container = styled.View`
  padding: 0 ${(props) => props.theme.space[4]};
  padding-top: ${(props) => props.theme.space[5]};
`;

const Heading = styled.Text`
  font-size: ${(props) => props.theme.fontSizes.h4};
  color: ${(props) => props.theme.colors.text.primary};
  font-weight: ${(props) => props.theme.fontWeights.medium};
`;

export const LoginScreen = () => {
  return (
    <SafeArea>
      <LoginContainer>
        <Container>
          <Spacer position="top" size="large" />
          <Heading>Welcome Back</Heading>
          <Spacer position="top" size="large" />
          <CustomInput title="Email" />
          <Spacer position="top" size="large" />
          <CustomInput title="password" />
          <Spacer position="top" size="large" />
          <CustomButtom>Login</CustomButtom>
        </Container>
      </LoginContainer>
    </SafeArea>
  );
};
