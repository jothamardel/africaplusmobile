export const colors = {
  brand: {
    primary: "#FF8000",
    secondary: "#3F3F3F",
    muted: "#AEAEAE",
  },
  ui: {
    primary: "#FF8000",
    secondary: "#3F3F3F",
    tertiary: "#F1F1F1",
    quaternary: "#FFFFFF",
    disabled: "#DEDEDE",
    error: "#D0421B",
    success: "#138000",
  },
  bg: {
    primary: "#FFFFFF",
    secondary: "#F1F1F1",
  },
  text: {
    primary: "#FF8000",
    secondary: "#757575",
    disabled: "#9C9C9C",
    inverse: "#FFFFFF",
    error: "#D0421B",
    success: "#138000",
  },
};
